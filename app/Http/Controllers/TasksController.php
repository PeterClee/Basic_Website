<?php

namespace App\Http\Controllers;

use App\Task;

class TasksController extends Controller
{
    public function show(Task $task){
      return $task;
      return view('taskitems.item', compact('task'));
    }
    public function index(){
      $tasks = Task::all();
      return view('taskitems.todo', compact('tasks'));
    }
    
}
