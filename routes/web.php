<?php

use App\Task;

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});
Route::get('/todo', 'TasksController@index');

Route::get('/item/{task}', 'TasksController@show');

Route::get('/{home?}', function ($home = null) {
    // return $home;
    return view('welcome');
});
Route::get('/', 'Postscontroller@index');
