<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      @yield('NamingConvention')
    </title>
    <link rel="stylesheet" href="/css/app.css">
  </head>
  <body>
  <div class="container">
    @yield('content')
  </div>

    @include('Inc.Sidebar')

    @yield('footer')
  </body>
</html>
