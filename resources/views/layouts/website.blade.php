
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <div class="TitleHolder">
    <title>
    @yield('NamingConvention')
    </title>
    </div>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="/css/cover.css" rel="stylesheet">
  </head>

  <body class="text-center">
   
    @yield('content')
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
        <div class="topbrand">
          <h3 class="masthead-brand"><div class="headertext">Clone</div></h3>
          <!--<img class="clonelanguage" src="/images/clonelanguage.png" alt="Clone"/></div>-->
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link" href="welcome.blade.php">Home</a>
            <a class="nav-link" href="#">Videos</a>
            <a class="nav-link" href="#">Music</a>
            <a class="nav-link" href="about.blade.php">About</a>
          </nav>
          <img class="clonelogo" src="/images/greyclonetranscropped.png" alt="ClonePicture">
        </div>
      </header>

      <main role="main" class="inner cover">
        <img class="youtubelogo" src="https://www.gstatic.com/youtube/img/branding/youtubelogo/svg/youtubelogo.svg" alt="ClonePicture">
        <br/>
           <a href="#" class="btn btn-lg btn-secondary">Visit Youtube</a>
        </p>
      </main>
      <footer class="mastfoot mt-auto">
        <div class="inner">
        @yield('footer')
        </div>
      </footer>
    </div>

      

    {{-- @include('Inc.Sidebar') --}}

    
  </body>
</html>
