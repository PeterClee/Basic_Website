@extends('Layouts.website')

@section('NamingConvention')
Todo List
@endsection

@section('content')
<h1>Todo list</h1>
<ul>
  @foreach ($tasks as $task)
  <li><h2><?php echo ucwords($task->title);?></h2>
    <span> <a href="/item/{{$task->id}}"> <?php echo ucwords($task->body); ?></a> - </span>
    <?php if ($task->completed == 0)
      echo "Not completed";
    elseif ($task->completed == 1)
      echo "Completed";
     ?>
  </li>
  @endforeach
</ul>
@endsection

@section('sidebar')
@parent
<p>This is an addition to the sidebar in the Todo page</p>
@endsection
