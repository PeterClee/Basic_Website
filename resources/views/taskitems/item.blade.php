@extends('Layouts.website')

@section('NamingConvention')
{{ $task->title}}
@endsection

@section('content')
<h1>Task Title: <?php echo ucwords($task->title); ?></h1>
<br>
<span>Description: <?php echo ucwords($task->body); ?></span>
<br>
 <span> Created at </span>
 <?php echo date('h:s a', strtotime($task->created_at)); ?>
 <span> on </span>
 <?php echo date('D dS', strtotime($task->created_at)); ?>
<span> of </span>
 <?php echo date('M Y', strtotime($task->created_at)); ?>
<br>
 <span>Completion Status: </span>
 <?php if ($task->completed == 0)
   echo "Not completed";
 elseif ($task->completed == 1)
   echo "Completed";
  ?>
@endsection

@section('sidebar')
@parent
<p>This is an addition to the sidebar in the about page</p>
@endsection
